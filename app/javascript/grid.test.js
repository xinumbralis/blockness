import { Block, BlockGrid, COLOURS, MAX_X, MAX_Y, getAdjacent, destroyShape, restack, reorder, refill } from './grid';
import { assert } from 'chai';

describe('Block', () => {
  it('should be created with correct coordinates and one of the valid colours', () => {
    let testCoords = [[1, 2], [4, 9], [0, 0]];

    testCoords.forEach(testCoord => {
      let block = new Block(...testCoord);
      assert.equal(block.x, testCoord[0], 'x is set correctly');
      assert.equal(block.y, testCoord[1], 'y is set correctly');
      assert.ok(COLOURS.indexOf(block.colour) > -1, 'colour is valid');
    });
  });
});

describe('BlockGrid', () => {
  describe('isValid', () => {
    it('should return if a coord is valid', () => {
      const blockGrid = new BlockGrid(3,3);
      assert.ok(blockGrid.isValid(2,2));
      assert.ok(blockGrid.isValid(1,2));
      assert.notOk(blockGrid.isValid(-1,2));
      assert.notOk(blockGrid.isValid(3,3));
    });
  });

  describe('getAdjacent', () => {
    it('should return adjacent real blocks', () => {
      const grid = new BlockGrid();
      assert.deepEqual(getAdjacent(grid, grid.grid, grid.grid[0][0]), [grid.grid[1][0], grid.grid[0][1]]);
      assert.deepEqual(getAdjacent(grid, grid.grid, grid.grid[3][3]), [grid.grid[3][2], grid.grid[4][3], grid.grid[3][4], grid.grid[2][3]]);
      assert.deepEqual(getAdjacent(grid, grid.grid, grid.grid[9][1]), [grid.grid[9][0], grid.grid[9][2], grid.grid[8][1]]);
    });
  });
  
  describe('destroyShape', () => {
    it('should mark a shape as destroyed', () => {
      const blockGrid = new BlockGrid(3,3);
      const grid = [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: false, colour: 'blue'}, { x: 0, y: 2, destroyed: false, colour: 'green'}],
        [{ x: 1, y: 0, destroyed: false, colour: 'blue'}, { x: 1, y: 1, destroyed: false, colour: 'red'}, { x: 1, y: 2, destroyed: false, colour: 'blue'}],
        [{ x: 2, y: 0, destroyed: false, colour: 'blue'}, { x: 2, y: 1, destroyed: false, colour: 'blue'}, { x: 2, y: 2, destroyed: false, colour: 'green'}]
      ];

      assert.deepEqual(
        destroyShape(blockGrid, grid[0][0])(grid),
        [
          [{ x: 0, y: 0, destroyed: true, colour: 'blue'}, { x: 0, y: 1, destroyed: true, colour: 'blue'}, { x: 0, y: 2, destroyed: false, colour: 'green'}],
          [{ x: 1, y: 0, destroyed: true, colour: 'blue'}, { x: 1, y: 1, destroyed: false, colour: 'red'}, { x: 1, y: 2, destroyed: false, colour: 'blue'}],
          [{ x: 2, y: 0, destroyed: true, colour: 'blue'}, { x: 2, y: 1, destroyed: true, colour: 'blue'}, { x: 2, y: 2, destroyed: false, colour: 'green'}]
        ]
      );
    });
  });
  
  describe('restack', () => {
    it('should drop the blocks down into the free slots', () => {
      const grid1 = [
        [{ x: 0, y: 0, destroyed: true, colour: 'blue'}, { x: 0, y: 1, destroyed: false, colour: 'blue'}, { x: 0, y: 2, destroyed: false, colour: 'green'}]
      ];
      const grid2 = [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: true, colour: 'blue'}, { x: 0, y: 2, destroyed: true, colour: 'green'}, { x: 0, y: 3, destroyed: false, colour: 'green'}]
      ];
      assert.deepEqual(restack(grid1), [
        [{ x: 0, y: 1, destroyed: false, colour: 'blue'}, { x: 0, y: 2, destroyed: false, colour: 'green'}],
      ]);
      assert.deepEqual(restack(grid2), [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 3, destroyed: false, colour: 'green'}]
      ]);
    });
  });
  
  describe('reorder', () => {
    it('should set the correct "y" prop on the moved blocks', () => {
      const grid = [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: true, colour: 'blue'}, { x: 0, y: 2, destroyed: true, colour: 'green'}, { x: 0, y: 3, destroyed: false, colour: 'green'}]
      ];
      const dropped = restack(grid);
      assert.deepEqual(reorder(dropped), [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: false, colour: 'green'}],
      ]);
    });
  });
  
  describe('refill', () => {
    it('should refill the columns with new disabled blocks', () => {
      const grid = [
        [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: true, colour: 'blue'}, { x: 0, y: 2, destroyed: true, colour: 'green'}, { x: 0, y: 3, destroyed: false, colour: 'green'}]
      ];
      assert.ok(refill(5, grid).length, 5);
      assert.ok(refill(100, grid).length, 100);
    });
  });
  
  describe('blockClicked', () => {
    const blockGrid = new BlockGrid(3,3);
    blockGrid.grid = [
      [{ x: 0, y: 0, destroyed: false, colour: 'blue'}, { x: 0, y: 1, destroyed: false, colour: 'blue'}, { x: 0, y: 2, destroyed: false, colour: 'green'}],
      [{ x: 1, y: 0, destroyed: false, colour: 'blue'}, { x: 1, y: 1, destroyed: false, colour: 'red'}, { x: 1, y: 2, destroyed: false, colour: 'blue'}],
      [{ x: 2, y: 0, destroyed: false, colour: 'blue'}, { x: 2, y: 1, destroyed: false, colour: 'blue'}, { x: 2, y: 2, destroyed: false, colour: 'green'}]
    ];
    blockGrid.clear = () => {};
    blockGrid.render = () => {};
    blockGrid.blockClicked(null, blockGrid.grid[2][0]);

    it('should drop blocks down', () => {
      assert.equal(blockGrid.grid[0][0].colour, 'green');

      assert.equal(blockGrid.grid[1][0].colour, 'red');
      assert.equal(blockGrid.grid[1][1].colour, 'blue');

      assert.equal(blockGrid.grid[2][0].colour, 'green');
    });

    it('should destroy and refill', () => {
      assert.notOk(blockGrid.grid[0][0].disabled);
      assert.ok(blockGrid.grid[0][1].disabled);
      assert.ok(blockGrid.grid[0][2].disabled);

      assert.notOk(blockGrid.grid[1][0].disabled);
      assert.notOk(blockGrid.grid[1][1].disabled);
      assert.ok(blockGrid.grid[1][2].disabled);

      assert.notOk(blockGrid.grid[2][0].disabled);
      assert.ok(blockGrid.grid[2][1].disabled);
      assert.ok(blockGrid.grid[2][2].disabled);
    });
  });

});
