import { range, duplicate, flow } from './helpers';
export const COLOURS = ['red', 'green', 'blue', 'yellow'];
export const MAX_X = 10;
export const MAX_Y = 10;
const GRID_EL = '#gridEl';

const markDestroyed = (block) => Object.assign(block, { destroyed: true });

export const destroyShape = (blockGrid, startingBlock) => grid => {
  let queue = [];
  let maxAttempts = 100;
    
  queue.push(Object.assign({}, startingBlock));
  while (queue.length && maxAttempts) {
    maxAttempts--;
    grid[queue[0].x][queue[0].y] = markDestroyed(queue[0]);
    const adj = getAdjacent(blockGrid, grid, queue[0]).filter(block => block.colour === startingBlock.colour && !block.destroyed);
    queue = queue.concat(adj);
    queue = queue.slice(1);
  }
  
  return grid;
}

export const getAdjacent = (blockGrid, grid, block) => {
  const adj = [];
  if (blockGrid.isValid(block.x, block.y - 1)) adj.push(grid[block.x][block.y - 1]);
  if (blockGrid.isValid(block.x + 1, block.y)) adj.push(grid[block.x + 1][block.y]);
  if (blockGrid.isValid(block.x, block.y + 1)) adj.push(grid[block.x][block.y + 1]);
  if (blockGrid.isValid(block.x - 1, block.y)) adj.push(grid[block.x - 1][block.y]);
  return adj;
}

export const restack = grid => grid.map(column => column.reduce((acc, cur) => cur.destroyed ? acc : acc.concat(cur), []));

export const reorder = grid => grid.map(column => column.map((block, i) => Object.assign(block, { y: i })));

export const refill = max_y => grid => grid.map((column, i) => column.concat(
  range(max_y - column.length).map((empty, j) => new Block(i, column.length + j, true)))
);

export class Block {
  constructor(x, y, disabled = false) {
    this.x = x;
    this.y = y;
    this.destroyed = false;
    this.disabled = disabled;
    this.colour = disabled ? null : COLOURS[Math.floor(Math.random() * COLOURS.length)];
  }
}

export class BlockGrid {
  constructor(max_x, max_y) {
    this.grid = [];
    this.max_x = max_x || MAX_X;
    this.max_y = max_y || MAX_Y;

    for (let x = 0; x < this.max_x; x++) {
      let col = [];
      for (let y = 0; y < this.max_y; y++) {
        col.push(new Block(x, y));
      }

      this.grid.push(col);
    }

    return this;
  }

  render(el = document.querySelector(GRID_EL)) {
    for (let x = 0; x < this.max_x; x++) {
      let id = 'col_' + x;
      let colEl = document.createElement('div');
      colEl.className = 'col';
      colEl.id = id;
      el.appendChild(colEl);

      for (let y = this.max_y - 1; y >= 0; y--) {
        let block = this.grid[x][y],
          id = `block_${x}x${y}`,
          blockEl = document.createElement('div');

        blockEl.id = id;
        blockEl.className = 'block';
        blockEl.style.background = (block.destroyed || block.disabled) ? 'gray' : block.colour;
        blockEl.addEventListener('click', evt => this.blockClicked(evt, block));
        colEl.appendChild(blockEl);
      }
    }

    return this;
  }
  
  clear(el = document.querySelector(GRID_EL)) {
    el.innerHTML = '';
  }

  blockClicked(e, block) {
    this.grid = flow(
      destroyShape(this, block),
      restack,
      reorder,
      refill(this.max_y)
    )(duplicate(this.grid));

    this.clear();
    this.render();
  }
  
  isValid(x, y) {
    return (x >= 0 && x < this.max_x) && (y >= 0 && y < this.max_y);
  }
}

window.addEventListener('DOMContentLoaded', () => {new BlockGrid().render()});
