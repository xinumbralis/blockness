export const range = i => [...Array(i).keys()];

export const duplicate = obj => JSON.parse(JSON.stringify(obj));

export const flow = (...functions) => (arg) => functions.reduce((acc, cur) => cur(acc), arg);
